const url = "http://localhost:3333/owner";

function getOwner() {
  axios
    .get(url)
    .then((response) => {
      const json = response.data;
      let tableTxt = "";

      json.forEach((owner) => {
        tableTxt += `<tr>`;
        tableTxt += `<td> ${owner.id} </td>`;
        tableTxt += `<td> ${owner.owner_name} </td>`;
        tableTxt += `<td> ${owner.cpf} </td>`;
        tableTxt += `<td> ${owner.address} </td>`;
        tableTxt += `<td> ${owner.number} </td>`;
        tableTxt += `<td> ${owner.city} </td>`;
        tableTxt += `<td> ${owner.phone} </td>`;
        tableTxt += `<td> ${owner.email} </td>`;
        tableTxt += `<td>
        <button 
          type='button' class='btn btn-warning badge-pill' data-toggle='modal' data-target='#detailsModal'>Details
        </button> 
        <button 
          type='button' class='btn btn-info badge-pill' data-toggle='modal' data-target='#editModal' 
          data-id="${owner.id}" data-name="${owner.owner_name}" data-cpf="${owner.cpf}" data-birth="${owner.birth}"  
          data-number="${owner.number}" data-city="${owner.city}" data-district="${owner.district}" 
          data-phone="${owner.phone}" data-email="${owner.email}" data-complement="${owner.complement}" 
          data-city="${owner.city}" data-state="${owner.state}" data-address="${owner.address}"
          onclick="getOwnerById(this)" >Edit
        </button> 
        <button 
          type='button' class='btn btn-danger badge-pill' data-toggle='modal' data-target='#excludeModal' 
          data-id="${owner.id}" onclick="deleteOwner(this)" id='excludeOwner'>Exclude
        </button></td>`;
        tableTxt += `</tr>`;
        owner.birth;
        owner.district;
        owner.complement;
        owner.city;
        owner.state;
      });
      document.getElementById("result").innerHTML = tableTxt;
    })
    .catch((error) => console.log(error));
}

getOwner();

function addOwner() {
  let owner_name = $("#inputName").val();
  let birth = $("#inputBirth").val();
  let cpf = $("#inputCpf").val();
  let email = $("#inputEmail").val();
  let phone = $("#inputPhone").val();
  let address = $("#inputAddress").val();
  let district = $("#inputDistrict").val();
  let number = $("#inputNumber").val();
  let complement = $("#inputComplement").val();
  let city = $("#inputCity").val();
  let state = $("#inputState").val();

  axios
    .post(url, {
      owner_name,
      birth,
      cpf,
      email,
      phone,
      address,
      district,
      number,
      complement,
      city,
      state,
    })
    .then((response) => {
      alert("Owner successfully registered");
    })
    .catch((error) => console.log(error));
    location.reload();
    return false;
}

function getOwnerById(id) {
  let data = $(id).attr("data-id");
  let ownerName = $(id).attr("data-name");
  let ownerBirth = $(id).attr("data-birth");
  let ownerCpf = $(id).attr("data-cpf");
  let ownerEmail = $(id).attr("data-email");
  let ownerPhone = $(id).attr("data-phone");
  let ownerAddress = $(id).attr("data-address");
  let ownerDistrict = $(id).attr("data-district");
  let ownerNumber = $(id).attr("data-number");
  let ownerComplement = $(id).attr("data-complement");
  let ownerCity = $(id).attr("data-city");
  let ownerState = $(id).attr("data-state");
  
  $("#updateName").val(ownerName);
  $("#updateBirth").val(ownerBirth);
  $("#updateCpf").val(ownerCpf);
  $("#updateEmail").val(ownerEmail);
  $("#updatePhone").val(ownerPhone);
  $("#updateAddress").val(ownerAddress);
  $("#updateDistrict").val(ownerDistrict);
  $("#updateNumber").val(ownerNumber);
  $("#updateComplement").val(ownerComplement);
  $("#updateCity").val(ownerCity);
  $("#updateState").val(ownerState);

  $("#btnEdit").click(function getOwnerById(id) {
  
    let owner_name = $("#updateName").val();
    let birth = $("#updateBirth").val();
    let cpf = $("#updateCpf").val();
    let email = $("#updateEmail").val();
    let phone = $("#updatePhone").val();
    let address = $("#updateAddress").val();
    let district = $("#updateDistrict").val();
    let number = $("#updateNumber").val();
    let complement = $("#updateComplement").val();
    let city = $("#updateCity").val();
    let state = $("#updateState").val();

    axios
      .put(`${url}/${data}`, {
        owner_name,
        birth,
        cpf,
        email,
        phone,
        address,
        district,
        number,
        complement,
        city,
        state,
      })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => console.log(error));
      location.reload();
      return false;
  });
};

function deleteOwner(id) {
    let result = confirm("Do you want delete this owner?");
    let data = $(id).attr("data-id");
    if(result) { 
      axios
        .delete(`${url}/${data}`)
        .then((response) => {
          console.log(response);
        })
        .catch((error) => console.log(error));
    };
    location.reload();
    return false;
};

